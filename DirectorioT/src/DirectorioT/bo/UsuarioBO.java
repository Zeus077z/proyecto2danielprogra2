/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DirectorioT.bo;
import DirectorioT.Entities.Cliente;
import DirectorioT.Entities.Entidad;
import DirectorioT.dao.UsuarioDAO;
import DirectorioT.Entities.Usuario;
import javax.swing.JOptionPane;

/**
 *
 * @author Allan Murillo
 */
public class UsuarioBO {
    //Aquí solo la funcion de insertar es funcional
    public boolean guardarCliente(Cliente usu, String repass) {
        validarCliente(usu, repass);
        usu.setContrasenna(Util.getSecurePassword(usu.getContrasenna()));
        if(usu.getId()> 0){
            JOptionPane.showMessageDialog(null,"Actualizar");
            return new UsuarioDAO().actualizarCliente(usu);          
        } else {
            JOptionPane.showMessageDialog(null,"Insertar");
           return new UsuarioDAO().insertarCliente(usu);
        }        
    }
    //Función de guardar entidad
    public boolean guardarEntidad(Entidad ent, String repass) {
        validarEntidad(ent, repass);
        ent.setContrasenna(Util.getSecurePassword(ent.getContrasenna()));
        return new UsuarioDAO().insertarEntidad(ent);
    }
    
    private void validarEntidad(Entidad ent, String repass) {
        if (ent == null) {
            throw new RuntimeException("Datos inválidos!!");
        }

        if (!ent.getContrasenna().equals(repass)) {
            throw new RuntimeException("Las contraseñas no coinciden");
        }

        if (ent.getContrasenna().length() < 8) {
            throw new RuntimeException("Contraseña debe tener mínimo 8 caracteres");
        }

        if (ent.getCedula_juridica().trim().isEmpty()) {
            throw new RuntimeException("La cédula jurídica es requerida");
        }

        if (ent.getNombre().trim().isEmpty()) {
            throw new RuntimeException("El nombre es requerido");
        }

        if (ent.getCorreo().isBlank()) { //Versión 11 
            //TODO: Validar el formato del correo
            throw new RuntimeException("El correo es requerido");
        }
        
        if (ent.getLongitud().isBlank()){
            throw new RuntimeException("La longitud es requerida");
        }
        
        if (ent.getLatitud().isBlank()){
            throw new RuntimeException("La latitud es requerida");
        }
        
        if (ent.getTipo().isBlank()) {
            throw new RuntimeException("Tipo es requerido");
        }
        
        if (ent.getUsuario().isBlank()){
            throw new RuntimeException("Usuario requerido");
        }
        
    }
    private void validarCliente(Cliente usu, String repass) {
        if (usu == null) {
            throw new RuntimeException("Datos inválidos!!");
        }

        if (!usu.getContrasenna().equals(repass)) {
            throw new RuntimeException("Las contraseñas no coinciden");
        }

        if (usu.getContrasenna().length() < 8) {
            throw new RuntimeException("Contraseña debe tener mínimo 8 caracteres");
        }

        if (usu.getCedula().trim().isEmpty()) {
            throw new RuntimeException("La cédula es requerida");
        }

        if (usu.getNombre().trim().isEmpty()) {
            throw new RuntimeException("El nombre es requerido");
        }

        if (usu.getCorreo().isBlank()) { //Versión 11 
            //TODO: Validar el formato del correo
            throw new RuntimeException("El correo es requerido");
        }
        
        if (usu.getLongitud().isBlank()){
            throw new RuntimeException("La longitud es requerida");
        }
        
        if (usu.getLatitud().isBlank()){
            throw new RuntimeException("La latitud es requerida");
        }
        
        if (usu.getTipo().isBlank()) {
            throw new RuntimeException("Tipo es requerido");
        }
        
        if (usu.getUsuario().isBlank()){
            throw new RuntimeException("Usuario requerido");
        }
        
    }

    public Cliente loginCliente(Cliente usu) {
        if (usu == null) {
            throw new RuntimeException("Datos inválidos!!");
        }
        if (usu.getCorreo().isBlank() && usu.getUsuario().isBlank()) { //Versión 11 
            throw new RuntimeException("El correo/usuario requeridos");
        }

        if (usu.getContrasenna().trim().isEmpty()) {
            throw new RuntimeException("Debe digitar una contraseña");
        }
        usu.setContrasenna(Util.getSecurePassword(usu.getContrasenna()));
        return new UsuarioDAO().loginCliente(usu);
    }
    
    public Entidad loginEntidad(Entidad ent) {
        if (ent == null) {
            throw new RuntimeException("Datos inválidos!!");
        }

        if (ent.getCorreo().isBlank() && ent.getUsuario().isBlank()) { //Versión 11 
            throw new RuntimeException("El correo/usuario requeridos");
        }

        if (ent.getContrasenna().trim().isEmpty()) {
            throw new RuntimeException("Debe digitar una contraseña");
        }
        ent.setContrasenna(Util.getSecurePassword(ent.getContrasenna()));
        return new UsuarioDAO().loginEntidad(ent);
    }
    
     public Usuario login(Usuario usu) {
        if (usu == null) {
            throw new RuntimeException("Datos inválidos!!");
        }
        if (usu.getContrasenna().trim().isEmpty()) {
            throw new RuntimeException("Debe digitar una contraseña");
        }
        usu.setContrasenna(Util.getSecurePassword(usu.getContrasenna()));
        return new UsuarioDAO().login(usu);
    }

}
