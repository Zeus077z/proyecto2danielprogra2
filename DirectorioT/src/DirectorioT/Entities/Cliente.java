/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DirectorioT.Entities;

/**
 *
 * @author Daniel
 */

//CLase cliente
public class Cliente extends Usuario{
    private int id;
    private String nombre;
    private String cedula;
    private String direccion;
    private String correo;
    private String telefono;
    private String latitud;
    private String longitud;

    public Cliente() {
    }

    public Cliente(int id, String nombre, String cedula, String direccion, String correo, String telefono, String latitud, String longitud, String usuario, String contrasenna, String tipo) {
        super(usuario, contrasenna, tipo);
        this.id = id;
        this.nombre = nombre;
        this.cedula = cedula;
        this.direccion = direccion;
        this.correo = correo;
        this.telefono = telefono;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    @Override
    public String toString() {
        return "Cliente{" + "id=" + id + ", nombre=" + nombre + ", cedula=" + cedula + ", direccion=" + direccion + ", correo=" + correo + ", telefono=" + telefono + ", latitud=" + latitud + ", longitud=" + longitud + '}';
    } 
      
}
