/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DirectorioT.Entities;

/**
 *
 * @author Daniel
 */
//Clase entidad
public class Entidad extends Usuario {
    private int id;
    protected String cedula_juridica;
    protected String nombre;
    protected String direccion;
    protected String correo;
    protected String telefono;
    protected String latitud;
    protected String longitud;

    public Entidad() {
    }
    
    public Entidad(int id, String cedula_juridica, String nombre, String direccion, String correo, String telefono, String latitud, String longitud, String usuario, String contrasenna, String tipo) {
        super(usuario, contrasenna, tipo);
        this.id = id;
        this.cedula_juridica = cedula_juridica;
        this.nombre = nombre;
        this.direccion = direccion;
        this.correo = correo;
        this.telefono = telefono;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCedula_juridica() {
        return cedula_juridica;
    }

    public void setCedula_juridica(String cedula_juridica) {
        this.cedula_juridica = cedula_juridica;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    @Override
    public String toString() {
        return "Entidad{" + "id=" + id + ", cedula_juridica=" + cedula_juridica + ", nombre=" + nombre + ", direccion=" + direccion + ", correo=" + correo + ", telefono=" + telefono + ", latitud=" + latitud + ", longitud=" + longitud + '}';
    }
    
    
    
}
