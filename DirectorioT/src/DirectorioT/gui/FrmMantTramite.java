/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DirectorioT.gui;

import DirectorioT.Entities.Tramite;
import DirectorioT.bo.TramiteBO;
import java.util.LinkedList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.sql.Date;

/**
 *
 * @author Allan Murillo
 */
public class FrmMantTramite extends javax.swing.JFrame {

    private TramiteBO abo;
    private JFrame parent;

    /**
     * Creates new form FrmMantProducto
     *
     * @param parent ventana padre
     */
    public FrmMantTramite(JFrame parent) {
        initComponents();
        setLocationRelativeTo(null);
        this.parent = parent;
        abo = new TramiteBO();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        lblError = new javax.swing.JLabel();
        txtFiltro = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbTramites = new javax.swing.JTable();
        btnAgregar = new javax.swing.JLabel();
        btnEliminar = new javax.swing.JLabel();
        btnEditar = new javax.swing.JLabel();
        btnRefrescar = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator6 = new javax.swing.JSeparator();
        jSeparator7 = new javax.swing.JSeparator();
        tbIna = new javax.swing.JRadioButton();
        rbAct = new javax.swing.JRadioButton();
        rbAll = new javax.swing.JRadioButton();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(800, 600));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jLabel1.setBackground(new java.awt.Color(102, 204, 255));
        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Trámites");

        lblError.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        lblError.setText(" ");

        txtFiltro.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtFiltro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtFiltroKeyReleased(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Buscar:");

        tbTramites.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbTramites.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Código", "Título", "Descripción", "Requisitos", "Precio", "Fecha de creación", "Fecha de última modificacion", "Activo", "Data"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Boolean.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbTramites.setDoubleBuffered(true);
        tbTramites.getTableHeader().setReorderingAllowed(false);
        tbTramites.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                seleccionar(evt);
            }
        });
        jScrollPane1.setViewportView(tbTramites);
        if (tbTramites.getColumnModel().getColumnCount() > 0) {
            tbTramites.getColumnModel().getColumn(0).setMinWidth(0);
            tbTramites.getColumnModel().getColumn(0).setPreferredWidth(0);
            tbTramites.getColumnModel().getColumn(0).setMaxWidth(0);
            tbTramites.getColumnModel().getColumn(1).setMinWidth(50);
            tbTramites.getColumnModel().getColumn(1).setPreferredWidth(50);
            tbTramites.getColumnModel().getColumn(1).setMaxWidth(50);
            tbTramites.getColumnModel().getColumn(2).setMinWidth(100);
            tbTramites.getColumnModel().getColumn(2).setPreferredWidth(100);
            tbTramites.getColumnModel().getColumn(2).setMaxWidth(200);
            tbTramites.getColumnModel().getColumn(3).setMinWidth(150);
            tbTramites.getColumnModel().getColumn(3).setPreferredWidth(150);
            tbTramites.getColumnModel().getColumn(4).setMinWidth(150);
            tbTramites.getColumnModel().getColumn(4).setPreferredWidth(150);
            tbTramites.getColumnModel().getColumn(5).setMinWidth(100);
            tbTramites.getColumnModel().getColumn(5).setPreferredWidth(100);
            tbTramites.getColumnModel().getColumn(5).setMaxWidth(100);
            tbTramites.getColumnModel().getColumn(6).setMinWidth(150);
            tbTramites.getColumnModel().getColumn(6).setPreferredWidth(150);
            tbTramites.getColumnModel().getColumn(7).setMinWidth(150);
            tbTramites.getColumnModel().getColumn(7).setPreferredWidth(150);
            tbTramites.getColumnModel().getColumn(8).setMinWidth(50);
            tbTramites.getColumnModel().getColumn(8).setPreferredWidth(50);
            tbTramites.getColumnModel().getColumn(8).setMaxWidth(50);
            tbTramites.getColumnModel().getColumn(9).setMinWidth(0);
            tbTramites.getColumnModel().getColumn(9).setPreferredWidth(0);
            tbTramites.getColumnModel().getColumn(9).setMaxWidth(0);
        }

        btnAgregar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnAgregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/DirectorioT/png/agregar32.png"))); // NOI18N
        btnAgregar.setDoubleBuffered(true);
        btnAgregar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnAgregarMouseClicked(evt);
            }
        });

        btnEliminar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/DirectorioT/png/ocultar32.png"))); // NOI18N
        btnEliminar.setDoubleBuffered(true);
        btnEliminar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnEliminarMouseClicked(evt);
            }
        });

        btnEditar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/DirectorioT/png/edit32.png"))); // NOI18N
        btnEditar.setDoubleBuffered(true);
        btnEditar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnEditarMouseClicked(evt);
            }
        });

        btnRefrescar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnRefrescar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/DirectorioT/png/refrescar32.png"))); // NOI18N
        btnRefrescar.setDoubleBuffered(true);
        btnRefrescar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnRefrescarMouseClicked(evt);
            }
        });

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jSeparator4.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jSeparator5.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jSeparator6.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jSeparator7.setOrientation(javax.swing.SwingConstants.VERTICAL);

        buttonGroup1.add(tbIna);
        tbIna.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbIna.setText("Inactivos");

        buttonGroup1.add(rbAct);
        rbAct.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        rbAct.setSelected(true);
        rbAct.setText("Activos");

        buttonGroup1.add(rbAll);
        rbAll.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        rbAll.setText("Todos");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Administrador R.M");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator2)
                    .addComponent(lblError, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 3, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRefrescar, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 128, Short.MAX_VALUE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(rbAll, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(rbAct, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(tbIna, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tbIna)
                            .addComponent(rbAct)
                            .addComponent(rbAll))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(btnRefrescar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEliminar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAgregar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEditar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator4, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator5)
                    .addComponent(jSeparator6, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator3, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator7, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 453, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblError, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnRefrescarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnRefrescarMouseClicked
        cargarTabla();
    }//GEN-LAST:event_btnRefrescarMouseClicked

    private void seleccionar(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_seleccionar
//        Tramite art = seleccionado();
//        Boolean marca = (Boolean) tbArticulos.getValueAt(tbArticulos.getSelectedRow(), 6);
//        if (art.isEstado() != marca && marca) {
//            activar();
//        } else if (art.isEstado() != marca && !marca) {
//            eliminar();
//        }
    }//GEN-LAST:event_seleccionar

    private void btnEliminarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEliminarMouseClicked
//        lblError.setText("");
//        try {
//            eliminar();
//        } catch (RuntimeException e) {
//            lblError.setText(e.getMessage());
//        } catch (Exception e) {
//            lblError.setText("Favor revisar los datos e intente nuevamente");
//        }
    }//GEN-LAST:event_btnEliminarMouseClicked

    private void btnEditarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEditarMouseClicked
//        lblError.setText("");
//        try {
//            editar();
//        } catch (RuntimeException e) {
//            lblError.setText(e.getMessage());
//        } catch (Exception e) {
//            lblError.setText("Favor revisar los datos e intente nuevamente");
//        }
    }//GEN-LAST:event_btnEditarMouseClicked

    private void btnAgregarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAgregarMouseClicked
        lblError.setText("");
        try {
            nuevo();
        } catch (RuntimeException e) {
            lblError.setText(e.getMessage());
        } catch (Exception e) {
            lblError.setText("Favor revisar los datos e intente nuevamente");
        }
    }//GEN-LAST:event_btnAgregarMouseClicked

    private void txtFiltroKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFiltroKeyReleased
//        String filtro = txtFiltro.getText().trim();
//        if (filtro.length() >= 3) {
//            cargarTabla();
//        } else {
//            DefaultTableModel modelo = (DefaultTableModel) tbArticulos.getModel();
//            modelo.setRowCount(0);
//        }
    }//GEN-LAST:event_txtFiltroKeyReleased

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        if (parent != null) {
            parent.setVisible(true);
        }
    }//GEN-LAST:event_formWindowClosed

    private void nuevo() {
        Tramite t = new Tramite();
        FrmTramite frm = new FrmTramite(this, t);
        setVisible(false);
        frm.setVisible(true);
    }

//    private void editar() {
//        Articulo art = seleccionado();
//        if (art.getId() <= 0) {
//            lblError.setText("Favor seleccionar un artículo");
//            return;
//        }
//        FrmArticulo frm = new FrmArticulo(this, art);
//        setVisible(false);
//        frm.setVisible(true);
//    }
//    
//    private void activar() {
//        Articulo art = seleccionado();
//        
//        String str = "¿Está seguro que desea Activar el artículo?\n"
//                + "- Código: %s\n"
//                + "- Nombre: %s\n"
//                + "- Descripción: %s";
//        String msj = String.format(str, art.getCodigo(), art.getNombre(), art.getDescripcion());
//        int res = JOptionPane.showConfirmDialog(this, msj, "Activar Artículo", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
//        if (res == JOptionPane.YES_OPTION) {
//            abo.activar(art.getId());
//        }
//        cargarTabla();
//    }
//    
//    private void eliminar() {
//        Articulo art = seleccionado();
//        
//        String str = "¿Está seguro que desea eliminar el artículo?\n"
//                + "- Código: %s\n"
//                + "- Nombre: %s\n"
//                + "- Descripción: %s";
//        String msj = String.format(str, art.getCodigo(), art.getNombre(), art.getDescripcion());
//        int res = JOptionPane.showConfirmDialog(this, msj, "Eliminar Artículo", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
//        if (res == JOptionPane.YES_OPTION) {
//            abo.eliminar(art.getId());
//        }
//        cargarTabla();
//    }
//    
//    private Articulo seleccionado() {
//        int row = tbArticulos.getSelectedRow();
//        int col = tbArticulos.getColumnCount() - 1;
//        if (row >= 0) {
//            Object o = tbArticulos.getModel().getValueAt(row, col);
//            if (o instanceof Impresion) {
//                return (Impresion) o;
//            } else if (o instanceof Foto) {
//                return (Foto) o;
//            } else if (o instanceof Evento) {
//                return (Evento) o;
//            } else if (o instanceof Articulo) {
//                return (Articulo) o;
//            }
//        }
//        return new Foto();
//    }
//    
    //Metodo de cargar tabla donde se mostraran los datos de la base de datos con respecto a los trámites
    public void cargarTabla() {
        lblError.setText("");
        try {
            DefaultTableModel modelo = (DefaultTableModel) tbTramites.getModel();
            modelo.setRowCount(0);
            char estado = rbAct.isSelected() ? 'A' : rbAll.isSelected() ? 'T' : 'I';
            String filtro = txtFiltro.getText().trim();
            if (estado == 'T' && filtro.length() == 0) {
                String msj = "Esta seguro que desea cargar todos los tramites";
                int res = JOptionPane.showConfirmDialog(this, msj, "Cargar Tramites", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                if (res == JOptionPane.NO_OPTION) {
                    return;
                }
            }
            LinkedList<Tramite> tramites = abo.cargarTramites(filtro, estado);
            for (Tramite a : tramites) {
                modelo.addRow(new Object[]{a.getId(),a.getCodigo(), a.getTitulo(), a.getDescripcion(), a.getRequisitos(),
                     "₡" + a.getPrecio(), a.getFecha_creacion(), a.getFecha_modificacion(), a.isEstado()});
                System.out.println(a.toString());
            }
            
        } catch (RuntimeException e) {
            lblError.setText(e.getMessage());
        } catch (Exception e) {
            lblError.setText("Favor revisar los datos e intente nuevamente");
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btnAgregar;
    private javax.swing.JLabel btnEditar;
    private javax.swing.JLabel btnEliminar;
    private javax.swing.JLabel btnRefrescar;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JLabel lblError;
    private javax.swing.JRadioButton rbAct;
    private javax.swing.JRadioButton rbAll;
    private javax.swing.JRadioButton tbIna;
    private javax.swing.JTable tbTramites;
    private javax.swing.JTextField txtFiltro;
    // End of variables declaration//GEN-END:variables
}
